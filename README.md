
# DemoShop Test Automation Framework


*A brief presentation of my project*

## This is my final project within the FastTrackIt Test Automation course.

### Software engineer in Test:  *`Iulia V. Stoica`*

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framework
    - PageObject Models

### How to access the project :
Note: All below commands should be run using git-bash

* Run command `git clone https://gitlab.com/iulia.stoica86/automation-demo-shop.git`
* Run `cd automation-demo-shop/`

### How to run the Automation Framework:
Note: All below commands should be run using git-bash

* Run command `mvn test`
* Run command `mvn allure:serve`
* Run command `mvn allure:report`

#### Page Objects
    -InventoryContainerTest
    -InventoryPage
    -LoginForm
    -Product
    -SwagLabLoginTest


