package org.fasttrackit;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;

public class InventoryContainerTest {
    SwagLabLoginTest swagLabLoginTest;

    @BeforeTest
    public void openInventory() {
        Configuration.browser = "firefox";
        open("https://www.saucedemo.com/");
        swagLabLoginTest = new SwagLabLoginTest();
        swagLabLoginTest.standardUserLoginTest();
    }

    @AfterMethod
    public void clean() {
        open("https://www.saucedemo.com/inventory.html");
    }

    @Test
    public void isLogoDisplay() {
        SelenideElement logo = $(".app_logo");
        assertTrue(logo.isDisplayed(), "After login, the logo appears in header");
    }

    @Test
    public void isSwagRobotDisplayed() {
        SelenideElement inventoryRobot = $(".peek");
        assertTrue(inventoryRobot.exists(), "The robot logo is displayed on the home page");
    }

    @Test
    public void shoppingCartDisplayed() {
        SelenideElement shoppingCartIcon = $(".shopping_cart_link");
        assertTrue(shoppingCartIcon.isDisplayed(), "The shopping cart icon is displayed in the right corner");
        shoppingCartIcon.click();
        SelenideElement shoppingCartContainer = $("#cart_contents_container");
        assertTrue(shoppingCartContainer.exists(), "The page is loading the shopping cart");
        SelenideElement continueShopping = $("#continue-shopping");
        continueShopping.click();
    }

    @Test
    public void burgerButtonDisplayed() {
        SelenideElement burgerButton = $("#react-burger-menu-btn");
        assertTrue(burgerButton.isDisplayed(), "The burger button appears in the left corner of the screen");
        burgerButton.click();
        SelenideElement burgerMenuList = $(".bm-item-list");
        assertTrue(burgerMenuList.isDisplayed(), "After click a burger menu list is visible");
    }

    @Test
    public void sortContainerDisplayed() {
        SelenideElement sortContainer = $(".product_sort_container");
        assertTrue(sortContainer.isDisplayed(), "The sort container is displayed below the cart icon");
        sortContainer.click();
        SelenideElement sortAToZ = $(".product_sort_container [value=az]");
        assertTrue(sortAToZ.exists(), "Default sorting is visible");
    }

    @Test
    public void inventoryContainerVisible() {
        SelenideElement productsAreDisplayed = $(".inventory_list");
        assertTrue(productsAreDisplayed.exists(), "All products are displayed on the base page");
    }

    @Test(dataProvider = "productData", dataProviderClass = Product.class)
    public void sauceLabsBackpack(Product product) {
        assertTrue(product.isDisplayed(), "The item is displayed");
        assertEquals(product.getPrice(), product.getExpectedPrice(), "Product" + product.getProductID() + "opened");
    }

    @Test
    public void footerFacebookLink() {
        SelenideElement facebookLink = $(".social_facebook");
        assertTrue(facebookLink.exists(), "The link is displayed in footer on the left side");
        facebookLink.click();
        assertEquals(facebookLink.lastChild().getAttribute("href"), "https://www.facebook.com/saucelabs");
    }

    @Test
    public void footerTwitterLink() {
        SelenideElement twitterLink = $(".social_twitter");
        assertTrue(twitterLink.exists(), "The link is displayed in footer on the left side");
        twitterLink.click();
        assertEquals(twitterLink.lastChild().getAttribute("href"), "https://twitter.com/saucelabs");
    }

    @Test
    public void footerLinkedinLink() {
        SelenideElement linkedinLink = $(".social_linkedin");
        assertTrue(linkedinLink.exists(), "The link is displayed in footer on the left side");
        linkedinLink.click();
        assertEquals(linkedinLink.lastChild().getAttribute("href"), "https://www.linkedin.com/company/sauce-labs/");
    }

    @Test
    public void tradeMarkLogo() {
        SelenideElement tradeMarkLogo = $(".footer_copy");
        assertTrue(tradeMarkLogo.exists(), "On the bottom of the footer, the trade mark is displayed");
        assertEquals(tradeMarkLogo.getText(), "© 2022 Sauce Labs. All Rights Reserved. Terms of Service | Privacy Policy");
    }

    @Test
    public void footerSwagRobot() {
        SelenideElement swagRobotGraphic = $(".footer_robot");
        assertTrue(swagRobotGraphic.exists(), "In footer , on the right corner, the robot logo is displayed");
    }

}


