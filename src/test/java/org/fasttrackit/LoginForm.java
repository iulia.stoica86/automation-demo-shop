package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginForm {

    private SelenideElement userName = $(".form_group [data-test=username]");
    private SelenideElement password = $("[data-test=password]");
    private SelenideElement loginButton = $("#login-button");

    public void fillUserName(String user) {
        userName.sendKeys(user);
    }
    public void fillPassword(String input){
        password.sendKeys(input);
    }
    public void clickLoginButton(){
        loginButton.click();
    }
}