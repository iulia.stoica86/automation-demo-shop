package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class InventoryPage {
    private SelenideElement menuButton = $(".bm-burger-button");
    private SelenideElement closeBurgerButton = $("#react-burger-cross-btn");


    public boolean menuButtonIsDisplayed() {
        return menuButton.exists();
    }
}