package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.DataProvider;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product {

    private final SelenideElement title;
    private final SelenideElement price;
    private final String productID;
    private final SelenideElement productDescription;
    private final String expectedProductDescription;
    public String getExpectedProductDescription;
    private String expectedProductPrice;

    public static final String SAUCE_LABS_BACKPACK_ID = "4";
    public static final String SAUCE_LABS_BIKE_ID = "0";
    public static final String SAUCE_LABS_BOLT_ID = "1";
    public static final String SAUCE_LABS_JACKET_ID = "5";
    public static final String SAUCE_LABS_ONESIE_ID = "2";
    public static final String TEST_ALLTHETHINGS_ID = "3";

    public static final Product SAUCE_LABS_BACKPACK = new Product(SAUCE_LABS_BACKPACK_ID, "$29.99", "carry.allTheThings()" +
            " with the sleek, " +
            "streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.");
    public static final Product SAUCE_LABS_BIKE = new Product(SAUCE_LABS_BIKE_ID, "$9.99", "A red light isn't the desired " +
            "state in testing but it sure helps when riding your bike at night. Water-resistant with 3 lighting modes, 1 AAA battery included.");
    public static final Product SAUCE_LABS_BOLT = new Product(SAUCE_LABS_BOLT_ID, "$15.99", "Get your testing superhero " +
            "on with the Sauce Labs bolt T-shirt. From American Apparel, 100% ringspun combed cotton, heather gray with red bolt.");
    public static final Product SAUCE_LABS_JACKET = new Product(SAUCE_LABS_JACKET_ID, "$49.99", "It's not every day that you " +
            "come across a midweight quarter-zip fleece jacket capable of handling everything from a relaxing day outdoors to a busy day at the office.");
    public static final Product SAUCE_LABS_ONESIE = new Product(SAUCE_LABS_ONESIE_ID, "$7.99","Rib snap infant onesie for" +
            " the junior automation engineer in development. Reinforced 3-snap bottom closure, two-needle hemmed sleeved and bottom won't unravel.");
    public static final Product TEST_ALLTHETHINGS = new Product(TEST_ALLTHETHINGS_ID,"$15.99","This classic Sauce Labs t-shirt " +
            "is perfect to wear when cozying up to your keyboard to automate a few tests. Super-soft and comfy ringspun combed cotton.");

    public Product(String productId, String price, String productDescription) {
        this.productID = productId;
        this.title = $(format("#item_%s_title_link", productId));
        this.price = title.parent().parent().$(".inventory_item_price");
        this.expectedProductPrice = price;
        this.productDescription = title.parent().$(".inventory_item_label");
        this.expectedProductDescription = productDescription;
    }

    public String getProductID() {
        return productID;
    }

    @DataProvider(name = "productData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[6][];
        products[4] = new Object[]{SAUCE_LABS_BACKPACK};
        products[0] = new Object[]{SAUCE_LABS_BIKE};
        products[1] = new Object[]{SAUCE_LABS_BOLT};
        products[5] = new Object[]{SAUCE_LABS_JACKET};
        products[2] = new Object[]{SAUCE_LABS_ONESIE};
        products[3] = new Object[]{TEST_ALLTHETHINGS};
        return products;
    }

    public boolean isDisplayed() {
        return title.exists();
    }

    public String getPrice() {
        return this.price.getText();
    }

    public String getExpectedPrice() {
        return this.expectedProductPrice;
    }

    public String getProductDescription() {
        return this.productDescription.getText();
    }

    public String getExpectedProductDescription() {
        return this.getExpectedProductDescription;
    }
}
