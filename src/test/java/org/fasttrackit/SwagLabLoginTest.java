package org.fasttrackit;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

public class SwagLabLoginTest {

    @BeforeTest
    public void openPage() {
        Configuration.browser = "firefox";
        open("https://www.saucedemo.com/");
    }

    @AfterMethod
    public void clean() {
        open("https://www.saucedemo.com/");
    }

    @Test
    public void isLogoDisplayed() {
        SelenideElement theLogoIsVisible = $(".login_logo");
        assertTrue(theLogoIsVisible.exists(), "The logo is visible on the home page");
    }

    @Test
    public void isSwagRobotDisplayed(){
        SelenideElement theRobotIsVisible = $(".bot_column");
        assertTrue(theRobotIsVisible.exists(), "The robot logo is displayed on the home page");
    }

    @Test
    public void theLoginModalIsDisplayed() {
        SelenideElement theLoginModalIsVisible = $(".login-box");
        assertTrue(theLoginModalIsVisible.exists(), "The login modal is visible on the home page");
    }

    @Test
    public void loginCredentialDisplayed(){
        SelenideElement loginCredential = $("#login_credentials");
        assertEquals(loginCredential.getText(),
                "Accepted usernames are:\n" +
                "standard_user\n" +
                "locked_out_user\n" +
                "problem_user\n" +
                "performance_glitch_user");
    }

    @Test
    public void standardUserLoginTest() {
        LoginForm loginForm = new LoginForm();
        loginForm.fillUserName("standard_user");
        loginForm.fillPassword("secret_sauce");
        loginForm.clickLoginButton();
        InventoryPage inventoryPage = new InventoryPage();
        assertTrue(inventoryPage.menuButtonIsDisplayed(), "Inventory page is displayed after successful login");
    }


}